#!/bin/bash
chmod 777 $(pwd)
echo $(id -u):$(id -g)
docker run -v $(pwd):/zap/wrk/:rw -t owasp/zap2docker-weekly zap-api-scan.py -t http://a90bbb8f29c534f1f84457c1b5b42169-65541168.us-east-2.elb.amazonaws.com:8080/welcome/v3/api-docs -f openapi -r zap_report.html
exit_code=$?

sudo mkdir -p owasp-zap-report
sudo mv zap_report.html owasp-zap-report

echo "Exit Code : $exit_code"

  if [[ ${exit_code} -ne 0 ]]; then
    echo "owasp-zap-report has either low/medium/high/risk. Please check the html report"
    exit 1;
    else
    echo "OWASP ZAP didnt reoport any risk"
  fi;	
